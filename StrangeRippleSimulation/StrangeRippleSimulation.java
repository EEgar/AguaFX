import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class StrangeRippleSimulation extends Application {
    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;
    //private static final double DAMPING = 0.995;
    private static final double DAMPING = 0.95;
    private static final double INV_DX_SQUARED = 1.0 / (10.0 * 10.0); //1.0 / (10.0 * 10.0);
    private static final double INV_DY_SQUARED = 1.0 / (10.0 * 10.0);
    private static final double DT = 0.1;
    private double[][] u = new double[WIDTH][HEIGHT];
    private double[][] uPrev = new double[WIDTH][HEIGHT];
    private double[][] v = new double[WIDTH][HEIGHT];
    private double[][] vPrev = new double[WIDTH][HEIGHT];
    private double[][] p = new double[WIDTH][HEIGHT];
    private double[][] pPrev = new double[WIDTH][HEIGHT];
    private double mouseX = 0.0;
    private double mouseY = 0.0;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        // Create the canvas
        Canvas canvas = new Canvas(WIDTH, HEIGHT);

        // Get the graphics context
        GraphicsContext gc = canvas.getGraphicsContext2D();

        // Create the scene
        Group root = new Group();
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        root.getChildren().add(canvas);

        // Add mouse movement listener
        scene.setOnMouseMoved(event -> {
            mouseX = event.getX();
            mouseY = event.getY();
        });

        // Add ESC key listener
        scene.setOnKeyPressed(event -> {
            //if (event.getCode().getName().equals("Escape")) {
            if (event.getCode().getName().equals("Esc")) {
                stage.close();
            }
        });

        // Add scene to stage
        stage.setScene(scene);

        // Show the stage
        stage.show();

        // Create the animation loop
        AnimationTimer animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                // Update the simulation
                update();

                // Clear the canvas
                gc.setFill(Color.WHITE);
                gc.fillRect(0, 0, WIDTH, HEIGHT);

                // Render the simulation
                render(gc);
            }
        };

        // Start the animation loop
        animationTimer.start();
    }

    private void update() {
        // Swap current and previous states
        double[][] tmp;
        tmp = uPrev;
        uPrev = u;
        u = tmp;
        tmp = vPrev;
        vPrev = v;
        v = tmp;
        tmp = pPrev;
        pPrev = p;
        p = tmp;

        // Calculate pressure gradients
        double[][] dpx = new double[WIDTH][HEIGHT];
        double[][] dpy = new double[WIDTH][HEIGHT];
        for (int i = 1; i < WIDTH - 1; i++) {
            for (int j = 1; j < HEIGHT - 1; j++) {
                dpx[i][j] = -0.5 * INV_DX_SQUARED * (p[i + 1][j] - p[i - 1][j]);
                dpy[i][j] = -0.5 * INV_DY_SQUARED * (p[i][j  + 1] - p[i][j - 1]);
            }
        }

        // Calculate horizontal and vertical velocities
        for (int i = 1; i < WIDTH - 1; i++) {
            for (int j = 1; j < HEIGHT - 1; j++) {
                u[i][j] = uPrev[i][j] - (uPrev[i][j] - uPrev[i - 1][j]) * DT * 10.0
                        - dpx[i][j] * DT;
                v[i][j] = vPrev[i][j] - (vPrev[i][j] - vPrev[i][j - 1]) * DT * 10.0
                        - dpy[i][j] * DT;
            }
        }

        // Apply damping
        for (int i = 1; i < WIDTH - 1; i++) {
            for (int j = 1; j < HEIGHT - 1; j++) {
                u[i][j] *= DAMPING;
                v[i][j] *= DAMPING;
            }
        }

        // Apply mouse disturbance
        int mx = (int) mouseX;
        int my = (int) mouseY;
        if (mx >= 0 && mx < WIDTH && my >= 0 && my < HEIGHT) {
            u[mx][my] = Math.sin(System.currentTimeMillis() / 2000.0) * 105.0;
            v[mx][my] = Math.cos(System.currentTimeMillis() / 2000.0) * 105.0;
        }

        // Calculate pressure
        for (int i = 1; i < WIDTH - 1; i++) {
            for (int j = 1; j < HEIGHT - 1; j++) {
                double dudx = (u[i + 1][j] - u[i - 1][j]) * 0.5 * INV_DX_SQUARED;
                double dvdy = (v[i][j + 1] - v[i][j - 1]) * 0.5 * INV_DY_SQUARED;
p[i][j] = (dudx + dvdy) / -2.0;
}
}

     // Apply boundary conditions
    for (int i = 1; i < WIDTH - 1; i++) {
        u[i][0] = 0.0;
        u[i][HEIGHT - 1] = 0.0;
        v[i][0] = 0.0;
        v[i][HEIGHT - 1] = 0.0;
        p[i][0] = p[i][1];
        p[i][HEIGHT - 1] = p[i][HEIGHT - 2];
    }
    for (int j = 1; j < HEIGHT - 1; j++) {
        u[0][j] = 0.0;
        u[WIDTH - 1][j] = 0.0;
        v[0][j] = 0.0;
        v[WIDTH - 1][j] = 0.0;
        p[0][j] = p[1][j];
        p[WIDTH - 1][j] = p[WIDTH - 2][j];
    }

    // Solve pressure Poisson equation using Gauss-Seidel relaxation
    for (int k = 0; k < 100; k++) {
        for (int i = 1; i < WIDTH - 1; i++) {
            for (int j = 1; j < HEIGHT - 1; j++) {
                p[i][j] = (pPrev[i + 1][j] + pPrev[i - 1][j] + pPrev[i][j + 1] + pPrev[i][j - 1]
                        - INV_DX_SQUARED * DT * DT * p[i][j]) * 0.25;
            }
        }
        // Apply boundary conditions to pressure
        for (int i = 1; i < WIDTH - 1; i++) {
            p[i][0] = p[i][1];
            p[i][HEIGHT - 1] = p[i][HEIGHT - 2];
        }
        for (int j = 1; j < HEIGHT - 1; j++) {
            p[0][j] = p[1][j];
            p[WIDTH - 1][j] = p[WIDTH - 2][j];
        }
    }
}

private void render(GraphicsContext gc) {
    // Loop over every pixel on the canvas
    for (int i = 0; i < WIDTH; i++) {
        for (int j = 0; j < HEIGHT; j++) {
            // Use the velocity at this pixel to calculate the color of the pixel
            // The faster the velocity, the brighter the color
            double speed = Math.sqrt(u[i][j] * u[i][j] + v[i][j] * v[i][j]);
            double brightness = Math.min(speed * 20.0, 1.0);
            Color color = Color.hsb(200.0, 1.0, brightness);

            // Set the pixel color on the canvas
            gc.setFill(color);
            gc.fillRect(i, j, 1, 1);

            // Add a line segment to represent the wave
            // This line segment will be added to the previous line segment drawn
            // for this pixel, creating a continuous wave
            gc.setStroke(color);
            gc.setLineWidth(2.0);
            double x1 = i - 0.4 * u[i][j];
            double y1 = j - 0.4 * v[i][j];
            double x2 = i + 0.4 * u[i][j];
            double y2 = j + 0.4 * v[i][j];
            gc.strokeLine(x1, y1, x2, y2);
        }
    }
}



}
