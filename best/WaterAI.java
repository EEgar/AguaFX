import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;

public class WaterAI extends JPanel implements Runnable, MouseListener, MouseMotionListener {

    private int width = 700, height = 400, hwidth, hheight; // Added hwidth and hheight variables
    private short[] ripplemap;
    private int[] texture;
    private int[] ripple;
    private int oldind, newind, mapind;
    private int riprad = 3;
    private Image im;

    private Thread animatorThread;
    private boolean frozen = false;

    private BufferedImage offscreenImage;
    private Graphics offscreenGraphics;

    public WaterAI() {
        addMouseListener(this);
        addMouseMotionListener(this);
        setPreferredSize(new Dimension(width, height));

        try {
            im = new ImageIcon(getClass().getResource("bg.jpg")).getImage();
        } catch (Exception e) {
            e.printStackTrace();
        }

        width = im.getWidth(this);
        height = im.getHeight(this);
        hwidth = width >> 1;
        hheight = height >> 1;

        offscreenImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        offscreenGraphics = offscreenImage.getGraphics();

        texture = new int[width * height];

        BufferedImage bufImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        bufImg.getGraphics().drawImage(im, 0, 0, width, height, null);
        texture = bufImg.getRGB(0, 0, width, height, texture, 0, width);

        ripplemap = new short[width * (height + 2) * 2];
        ripple = new int[width * height];

        oldind = width;
        newind = width * (height + 3);

        for (int i = 0; i < width * height; i++) {
            ripple[i] = texture[i];
        }

        animatorThread = new Thread(this);
        animatorThread.start();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            WaterAI water = new WaterAI();
            JFrame frame = new JFrame("Water Simulation");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setResizable(true);
            frame.add(water);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        });
    }

    public void start() {
        frozen = false;
    }

    public void stop() {
        frozen = true;
    }

    public void mousePressed(MouseEvent e) {
        if (frozen) {
            start();
        } else {
            stop();
        }
    }

    public void mouseMoved(MouseEvent e) {
        disturb(e.getX(), e.getY());
    }

    public void run() {
        while (true) {
            if (!frozen) {
                newframe();
                repaint();
            }

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void disturb(int dx, int dy) {
        for (int j = dy - riprad; j < dy + riprad; j++) {
            for (int k = dx - riprad; k < dx + riprad; k++) {
                if (j >= 0 && j < height && k >= 0 && k < width) {
                    ripplemap[oldind + (j * width) + k] += 512;
                }
            }
        }
    }

    private void newframe() {
        int i = oldind;
        oldind = newind;
        newind = i;

        i = 0;
        mapind = oldind;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                short data = (short) ((ripplemap[mapind - width] + ripplemap[mapind + width]
                        + ripplemap[mapind - 1] + ripplemap[mapind + 1]) >> 1);
                data -= ripplemap[newind + i];
                data -= data >> 5;
                ripplemap[newind + i] = data;

                data = (short) (1024 - data);

                int a = ((x - hwidth) * data / 1024) + hwidth;
                int b = ((y - hheight) * data / 1024) + hheight;

                if (a >= width) a = width - 1;
                if (a < 0) a = 0;
                if (b >= height) b = height - 1;
                if (b < 0) b = 0;

                ripple[i] = texture[a + (b * width)];
                mapind++;
                i++;
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        offscreenGraphics.clearRect(0, 0, width, height);
        for (int i = 0; i < ripple.length; i++) {
            int x = i % width;
            int y = i / width;
            offscreenGraphics.setColor(new Color(ripple[i]));
            offscreenGraphics.fillRect(x, y, 1, 1);
        }
        g.drawImage(offscreenImage, 0, 0, this);
    }

    // Other MouseListener and MouseMotionListener methods...
    public void mouseDragged(MouseEvent e) {
        // Not used, but required by MouseMotionListener
    }

    public void mouseReleased(MouseEvent e) {
        // Not used, but required by MouseListener
    }

    public void mouseClicked(MouseEvent e) {
        // Not used, but required by MouseListener
    }

    public void mouseExited(MouseEvent e) {
        // Not used, but required by MouseListener
    }

    public void mouseEntered(MouseEvent e) {
        // Not used, but required by MouseListener
    }
}
