import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import javax.imageio.ImageIO;
import java.io.File;
import java.awt.image.BufferedImage;
import java.awt.Robot;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Rectangle;



public class WatRip extends JPanel {
    private ArrayList<Ripple> ripples = new ArrayList<Ripple>();
    private static final int RIPPLE_RADIUS = 50;
    private static final int RIPPLE_DURATION = 1000;

    public WatRip() {
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                ripples.add(new Ripple(e.getPoint()));
                repaint();
            }
        });

        addMouseMotionListener(new MouseAdapter() {
            private Point lastPoint = null;
            public void mouseMoved(MouseEvent e) {
                if (lastPoint == null || e.getPoint().distance(lastPoint) > 0.1) {
                    ripples.add(new Ripple(e.getPoint()));
                    repaint();
                    lastPoint = e.getPoint();
                }
            }
        });

        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    for (int i = 0; i < ripples.size(); i++) {
                        Ripple ripple = ripples.get(i);
                        ripple.update();
                        if (ripple.isDone()) {
                            ripples.remove(i);
                            i--;
                        }
                    }
                    repaint();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        
        //add key shot
        addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_F12) {
					captureScreen();
				}
			}
		});
		setFocusable(true);

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Create a gradient background
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint gp = new GradientPaint(0, 0, new Color(0, 191, 255), getWidth(), getHeight(), new Color(135, 206, 250));
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        // Draw ripples
        List<Ripple> ripplesCopy = new ArrayList<>(ripples); // make a copy of the ripples list
        for (Ripple ripple : ripplesCopy) {
            ripple.update();
            ripple.draw(g);
            if (ripple.isDone()) {
                ripples.remove(ripple); // remove the ripple from the original list
            }
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Water Ripples");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.getContentPane().add(new WatRip());
                frame.pack();
                frame.setVisible(true);
            }
        });
    }

    private class Ripple {
        private Point center;
        private long startTime;
        private long duration = RIPPLE_DURATION;

        public Ripple(Point center) {
            this.center = center;
            this.startTime = System.currentTimeMillis();
        }

        public void update() {
            long currentTime = System.currentTimeMillis();
            long elapsedTime = currentTime - startTime;
            //if (elapsedTime >= duration) {
            //    duration = 0;
            //}
            duration = Math.max(0, RIPPLE_DURATION - elapsedTime);

        }

        public void draw(Graphics g) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            
            float alpha = 1.0f - ((float) duration / (float) RIPPLE_DURATION);
            Color color = new Color(51, 255, 255, (int) (alpha * 255)); // TEAL color (RGB values: 0, 128, 128)
            g2d.setColor(color);
            
            int radius = (int) (RIPPLE_RADIUS * ((float) duration / (float) RIPPLE_DURATION));
            int diameter = radius * 2;
            int x = center.x - radius;
            int y = center.y - radius;
            g2d.fillOval(x, y, diameter, diameter);
        }


        public boolean isDone() {
            return duration == 0;
        }
    }
    
    
    //shot make
    private void captureScreen() {
    try {
        Robot robot = new Robot();
        Rectangle screenRect = new Rectangle(getLocationOnScreen(), getSize());
        BufferedImage screenImg = robot.createScreenCapture(screenRect);
        String fileName = "screenshot_" + System.currentTimeMillis() + ".png";
        File output = new File(fileName);
        ImageIO.write(screenImg, "png", output);
        System.out.println("Screenshot saved to " + fileName);
    } catch (Exception ex) {
        ex.printStackTrace();
    }
}

    
}
