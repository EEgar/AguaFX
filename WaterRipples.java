import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;



/**
 * if (ripple != null) {
 *   ripple.update();
* }
*Alternatively, you may need to check 
* if any other variables in your code might be null, and handle them 
* appropriately to prevent the NullPointerException.
*
*It's also possible that the error is related to concurrency issues, 
* as Swing is not thread-safe and updating the UI from multiple threads 
* can lead to unpredictable behavior. Make sure that you are only updating 
* the UI from the Event Dispatch Thread (EDT), which is the thread that handles 
* all user interface updates in Swing. You can use the SwingUtilities.invokeLater() 
* method to execute code on the EDT.
 * 
 */

public class WaterRipples extends JPanel {
    private ArrayList<Ripple> ripples = new ArrayList<Ripple>();
    private static final int RIPPLE_RADIUS = 50;
    private static final int RIPPLE_DURATION = 1000;

    public WaterRipples() {
        setBackground(Color.WHITE);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                ripples.add(new Ripple(e.getPoint()));
                repaint();
            }
        });
        
        
        
/*
        addMouseMotionListener(new MouseAdapter() {
            public void mouseMoved(MouseEvent e) {
                ripples.add(new Ripple(e.getPoint()));
                repaint();
            }
        });
*/
addMouseMotionListener(new MouseAdapter() {
    private Point lastPoint = null;
    public void mouseMoved(MouseEvent e) {
        if (lastPoint == null || e.getPoint().distance(lastPoint) > 0.1) {
            ripples.add(new Ripple(e.getPoint()));
            repaint();
            lastPoint = e.getPoint();
        }
    }
});



        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    for (int i = 0; i < ripples.size(); i++) {
                        Ripple ripple = ripples.get(i);
                        ripple.update();
                        if (ripple.isDone()) {
                            ripples.remove(i);
                            i--;
                        }
                    }
                    repaint();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
/*
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Ripple ripple : ripples) {
            ripple.draw(g);
        }
    }
*/
public void paintComponent(Graphics g) {
    super.paintComponent(g);

    List<Ripple> ripplesCopy = new ArrayList<>(ripples); // make a copy of the ripples list

    for (Ripple ripple : ripplesCopy) {
        ripple.update();
        ripple.draw(g);
        if (ripple.isDone()) {
            ripples.remove(ripple); // remove the ripple from the original list
        }
    }
}




    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("Water Ripples");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.getContentPane().add(new WaterRipples());
                frame.pack();
                frame.setVisible(true);
            }
        });
    }

    private class Ripple {
        private Point center;
        private long startTime;
        private long duration = RIPPLE_DURATION;

        public Ripple(Point center) {
            this.center = center;
            this.startTime = System.currentTimeMillis();
        }

        public void update() {
            long currentTime = System.currentTimeMillis();
            long elapsedTime = currentTime - startTime;
            if (elapsedTime >= duration) {
                duration = 0;
            }
        }

        public void draw(Graphics g) {
            long elapsedTime = System.currentTimeMillis() - startTime;
            double percentComplete = (double) elapsedTime / duration;
            int radius = (int) (RIPPLE_RADIUS * percentComplete);
            int alpha = (int) (255 * (1 - percentComplete));
            //g.setColor(new Color(0, 0, 255, alpha)); fix this
            
            int alphaInt = (int) Math.max(0, Math.min(255, alpha));
			g.setColor(new Color(0, 0, 255, alphaInt));
            
            g.drawOval(center.x - radius, center.y - radius, radius * 2, radius * 2);
        }


/*
		public void draw(Graphics2D g2d) {
    if (alpha > 0) {
        alpha -= 0.05;
        int alphaInt = (int) Math.max(0, Math.min(255, alpha));
        g2d.setColor(new Color(0, 0, 255, alphaInt));
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha / 255));
        g2d.fillOval(x - radius, y - radius, radius * 2, radius * 2);
    }
}
*/



        public boolean isDone() {
            return duration == 0;
        }
    }
}
