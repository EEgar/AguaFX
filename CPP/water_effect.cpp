#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

const int WIDTH = 800;
const int HEIGHT = 600;
const float DAMPING = 0.99f;
const int MAX_DROP_RADIUS = 10;
const int MAX_DROP_SPEED = 5;

class WaterEffect {
public:
    WaterEffect();

    void render(sf::RenderWindow& window);
    void disturb(int dx, int dy);
    void newFrame();

private:
    std::vector<float> u;
    std::vector<float> u_prev;
    float decay = 0.99f;
};

WaterEffect::WaterEffect() : u(WIDTH * HEIGHT, 0.0f), u_prev(WIDTH * HEIGHT, 0.0f) {
}

void WaterEffect::render(sf::RenderWindow& window) {
    sf::Image image;
    image.create(WIDTH, HEIGHT, sf::Color::Black);

    for (int y = 1; y < HEIGHT - 1; y++) {
        for (int x = 1; x < WIDTH - 1; x++) {
            int idx = x + y * WIDTH;
            int colorValue = static_cast<int>((u[idx] + 1.0f) * 127.0f);
            if (colorValue < 0) {
                colorValue = 0;
            } else if (colorValue > 255) {
                colorValue = 255;
            }
            image.setPixel(x, y, sf::Color(colorValue, colorValue, colorValue));
        }
    }

    sf::Texture texture;
    texture.loadFromImage(image);

    sf::Sprite sprite(texture);
    window.draw(sprite);
    window.display();
}

void WaterEffect::disturb(int dx, int dy) {
    // Simulate a disturbance at the specified location
    u[dx + dy * WIDTH] = 10.0f;
}

void WaterEffect::newFrame() {
    // Simulate wave propagation using the 2D wave equation
    for (int y = 1; y < HEIGHT - 1; y++) {
        for (int x = 1; x < WIDTH - 1; x++) {
            int idx = x + y * WIDTH;
            u[idx] = (
                (u_prev[x + (y - 1) * WIDTH] +
                u_prev[x + (y + 1) * WIDTH] +
                u_prev[(x - 1) + y * WIDTH] +
                u_prev[(x + 1) + y * WIDTH]) / 2 - u[idx]) * decay;
        }
    }

    // Swap current and previous waves
    std::swap(u, u_prev);
}

int main() {
    sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "Water Effect");

    WaterEffect waterEffect;

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            } else if (event.type == sf::Event::MouseButtonPressed) {
                waterEffect.disturb(event.mouseButton.x, event.mouseButton.y);
            }
        }

        waterEffect.newFrame();

        window.clear();
        waterEffect.render(window);
    }

    return 0;
}
