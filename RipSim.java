import javafx.animation.*;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.*;
import javafx.scene.shape.Circle;
import javafx.stage.*;
import javafx.util.Duration;

//import java.awt.*;

public class RipSim extends Application {
  private static final Paint SCENE_FILL = new RadialGradient(
    0, 0, 300, 300, 500, false, CycleMethod.NO_CYCLE, 
    FXCollections.observableArrayList(new Stop(0, Color.web("#0988B1",1.0)), new Stop(1, Color.web("#08A7A3",1.0)) )//BG COLOR
  );

  @Override public void start(Stage stage) {
    final RippleGenerator rippler = new RippleGenerator();

    final Scene scene = new Scene(rippler, 600, 400, SCENE_FILL);

    scene.setOnMousePressed(new EventHandler<MouseEvent>() { //click and hold
      @Override public void handle(MouseEvent event) {
        rippler.setGeneratorCenterX(event.getSceneX());
        rippler.setGeneratorCenterY(event.getSceneY());
        rippler.createRipple();
        rippler.startGenerating();
      }
    });

    scene.setOnMouseDragged(new EventHandler<MouseEvent>() { //moving pressed
      @Override public void handle(MouseEvent event) {
        rippler.setGeneratorCenterX(event.getSceneX());
        rippler.setGeneratorCenterY(event.getSceneY());
      }
    });

    scene.setOnMouseReleased(new EventHandler<MouseEvent>() { //unclick
      @Override public void handle(MouseEvent event) {
        rippler.stopGenerating();
      }
    });

    scene.setOnMouseEntered(new EventHandler<MouseEvent>() { // hover
      @Override public void handle(MouseEvent event) {
        rippler.setGeneratorCenterX(event.getSceneX());
        rippler.setGeneratorCenterY(event.getSceneY());
        rippler.createRipple();
        rippler.startGenerating();
      }
    });

    scene.setOnMouseMoved(new EventHandler<MouseEvent>() { //moving cursor
      @Override public void handle(MouseEvent event) {
        rippler.setGeneratorCenterX(event.getSceneX());
        rippler.setGeneratorCenterY(event.getSceneY());
      }
    });

    scene.setOnMouseExited(new EventHandler<MouseEvent>() { //remove cursor from window
      @Override public void handle(MouseEvent event) {
        rippler.stopGenerating();
      }
    });

    stage.setTitle("Click, hold, Hover");
    stage.setScene(scene);
    stage.setResizable(true);

    stage.show();
  }

  public static void main(String[] args) { launch(args); }
}

/**
 * Generates ripples on the screen every 0.5 seconds or whenever
 * the createRipple method is called. Ripples grow and fade out
 * over 3 seconds
 */
class RippleGenerator extends Group {
  private class Ripple extends Circle {
    Timeline animation = new Timeline(
      new KeyFrame(Duration.ZERO,       new KeyValue(radiusProperty(),  0)),
      new KeyFrame(Duration.seconds(10), new KeyValue(opacityProperty(), 1)),//
      new KeyFrame(Duration.seconds(3), new KeyValue(radiusProperty(),  200)),//3 100
      new KeyFrame(Duration.seconds(3), new KeyValue(opacityProperty(), 0))//3
    );

    private Ripple(double centerX, double centerY) {
      super(centerX, centerY, 0, null);
      setStroke(Color.rgb(126, 202, 230)); //round ripple color
    }
  }

  private double generatorCenterX = 100.0;
  private double generatorCenterY = 100.0;

  private Timeline generate = new Timeline(
      new KeyFrame(Duration.seconds(0.2), new EventHandler<ActionEvent>() { /////0.5 While hold
        @Override public void handle(ActionEvent event) {
          createRipple();
        }
      }
    )
  );

  public RippleGenerator() {
    generate.setCycleCount(Timeline.INDEFINITE);
  }

  public void createRipple() {
    final Ripple ripple = new Ripple(generatorCenterX, generatorCenterY);
    getChildren().add(ripple);
    ripple.animation.play();

    Timeline remover = new Timeline(
      new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
        @Override public void handle(ActionEvent event) {
          getChildren().remove(ripple);
          ripple.animation.stop(); 
        }
      })  
    );
    remover.play();
  }

  public void startGenerating() {
    generate.play();
  }

  public void stopGenerating() {
    generate.stop();
  }

  public void setGeneratorCenterX(double generatorCenterX) {
    this.generatorCenterX = generatorCenterX;
  }

  public void setGeneratorCenterY(double generatorCenterY) {
    this.generatorCenterY = generatorCenterY;
  }
}

//https://stackoverflow.com/questions/16961352/water-2d-wave-effect-in-javafx